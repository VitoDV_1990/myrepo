package com.Design_patterns.iterator;

import java.util.ArrayList;

public class ConcreteIterator implements MyIterator {
	
	private ArrayList arrayList;
    private int current = -1;
 
    public ConcreteIterator( ArrayList arrayList ) {
        this.arrayList = arrayList;
    }
 
   
    public String next() {
        current++;
        return (String) arrayList.get(current);
    }
 
    public boolean hasNext() {
        return (current+1) < arrayList.size();
    }

}
