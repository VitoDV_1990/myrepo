package com.Design_patterns.iterator;

import java.util.ArrayList;

public class ConcreteAggregate implements Aggregate {
	
	private ArrayList arrayList;
	 
    public ConcreteAggregate() {
        arrayList = new ArrayList();
    }
 
    public MyIterator createIterator() {
        return new ConcreteIterator( arrayList );
    }
 
    public void add(String item) {
        arrayList.add( item );
    }

}
