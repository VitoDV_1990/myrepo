package com.Design_patterns.iterator;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Aggregate aggregate = new ConcreteAggregate();
        aggregate.add("item1");
        aggregate.add("item2");
 
        MyIterator iterator = aggregate.createIterator();
        while ( iterator.hasNext() ) {
            System.out.println( iterator.next() );
        }

	}

}
