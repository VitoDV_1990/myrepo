package com.Design_patterns.iterator;

public interface Aggregate {
	
	public MyIterator createIterator();
	
    public void add(String item);

}
