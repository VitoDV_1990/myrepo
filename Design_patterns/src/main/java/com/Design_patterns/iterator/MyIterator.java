package com.Design_patterns.iterator;

public interface MyIterator {
	
	public boolean hasNext();
	
    public Object next();

}
