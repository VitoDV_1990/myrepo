package com.Design_patterns.chainOfResponsibility;

public class CreditRequestHandlerException extends Exception {
	
	public CreditRequestHandlerException() {
		super( "No handler found to forward the request." );
	}

}
