package com.Design_patterns.chainOfResponsibility;

public class Customer {
	
	public void requestCredit( CreditRequestHandler crHandler, int amount ) throws CreditRequestHandlerException {
		crHandler.creditRequest( amount );
	}

}
