package com.Design_patterns.visitor;

public interface Visitable {
	
	public void accept( Visitor visitor ) ;

}
