package com.Design_patterns.dependencyInjection;

public interface Report {
	
	public void generate(String data);
	
	public void saveToFile();

}
