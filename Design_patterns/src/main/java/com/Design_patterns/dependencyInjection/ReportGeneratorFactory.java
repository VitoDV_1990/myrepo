package com.Design_patterns.dependencyInjection;

public class ReportGeneratorFactory {
	
	public static ReportGenerator createTxtReportGenerator() {
		 
	    ReportGenerator rg = new ReportGenerator();
	     
	    rg.setReport(new TxtReport("/report")); 
	 
	    return rg;
	  }

}
