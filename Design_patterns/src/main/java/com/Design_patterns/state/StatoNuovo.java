package com.Design_patterns.state;

public class StatoNuovo implements Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato) {
        if (stato.equals("in_prenotazione"))
           ordine.setStatoOrdine(new StatoInPrenotazione());
        else if (stato.equals("in_corso"))
           ordine.setStatoOrdine(new StatoInCorso());
    }

}
