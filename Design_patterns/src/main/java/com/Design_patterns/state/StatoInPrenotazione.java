package com.Design_patterns.state;

public class StatoInPrenotazione implements Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato) {
        if (stato.equals("in_corso"))
           ordine.setStatoOrdine(new StatoInCorso());
    }

}
