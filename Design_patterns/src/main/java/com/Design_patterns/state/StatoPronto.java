package com.Design_patterns.state;

public class StatoPronto implements Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato) {
        if (stato.equals("in_partenza"))
            ordine.setStatoOrdine(new StatoInPartenza());
    }

}
