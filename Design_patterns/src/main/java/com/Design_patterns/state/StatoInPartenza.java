package com.Design_patterns.state;

public class StatoInPartenza implements Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato) {
        if (stato.equals("spedito"))
            ordine.setStatoOrdine(new StatoSpedito());
    }

}
