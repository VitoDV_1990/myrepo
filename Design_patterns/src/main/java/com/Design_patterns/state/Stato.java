package com.Design_patterns.state;

public interface Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato);

}
