package com.Design_patterns.state;

public class StatoEsaurito implements Stato {
	
	public void gestioneStatoOrdine(Ordine ordine, String stato) {
        if (stato.equals("cancellato"))
            ordine.setStatoOrdine(new StatoCancellato());
    }

}
