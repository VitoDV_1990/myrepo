package com.Design_patterns.abstractFactory;

public interface DevicesFactory {
	
	public Player createPlayer();
	public Recorder createRecorder();
	public Media createMedia();

}
