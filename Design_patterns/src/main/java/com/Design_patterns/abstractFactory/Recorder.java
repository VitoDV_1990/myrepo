package com.Design_patterns.abstractFactory;

public interface Recorder {
	
	public void accept( Media med );
	
	public void record( String sound );

}
