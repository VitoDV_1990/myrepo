package com.Design_patterns.abstractFactory;

public interface Player {
	
	public void accept( Media med );
	
	public void play( );

}
