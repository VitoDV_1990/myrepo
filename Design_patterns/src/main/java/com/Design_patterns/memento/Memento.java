package com.Design_patterns.memento;

public interface Memento {
	
	public void restoreState();

}
