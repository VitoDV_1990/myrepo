package com.Design_patterns.decorator;

public interface Employee {
	
	public String getName();
	
	public String getOffice();
	
	public void whoIs();

}
