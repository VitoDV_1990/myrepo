package com.Design_patterns.interpreter;

public class VariableExpression implements BooleanExpression {
	
	private String varName;
	
	public VariableExpression(String name) {
		varName = name;
	}
	
	public boolean evaluate(Context context) {
		return context.lookup( varName );
	}
	
	public String getVarName() {
		return varName;
	}

}
