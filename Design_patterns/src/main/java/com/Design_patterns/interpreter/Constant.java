package com.Design_patterns.interpreter;

public class Constant implements BooleanExpression {
	
	boolean value;
	
	public Constant(boolean val) {
		value = val;
	}
	
	public boolean evaluate(Context context) {
		return value;
	}

}
