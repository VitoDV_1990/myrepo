package com.Design_patterns.interpreter;

public interface BooleanExpression {
	
	public boolean evaluate( Context context );

}
