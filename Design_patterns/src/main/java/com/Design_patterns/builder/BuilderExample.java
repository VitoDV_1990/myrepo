package com.Design_patterns.builder;

public class BuilderExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String swIngCourseModel = (String) ERHardCodedDirector.getModel( new OrientedERBuilder() );
		System.out.println( swIngCourseModel );
		
		ERModel dbCourseModel = (ERModel) ERHardCodedDirector.getModel( new NotOrientedERBuilder() );
		dbCourseModel.showStructure();
	}

}
