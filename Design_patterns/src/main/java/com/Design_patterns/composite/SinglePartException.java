package com.Design_patterns.composite;

public class SinglePartException extends Exception {
	
	public SinglePartException( ) {
		super( "Not supported method" );
	}

}
