package com.Design_patterns.adapter;

public class ClassAdapterExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Polygon block = new RectangleClassAdapter();
		
		block.setId( "Demo" );
		block.define( 3 , 4 , 10, 20, "RED" );
		
		System.out.println( "The area of "+ block.getId() + " is "+ block.getSurface() + ", and it�s " + block.getColor() );
	}

}
