package com.Design_patterns.strategy;

public interface ArrayDisplayFormat {
	
	public void printData( int[] arr );

}
