package com.Design_patterns.command;

public class TVPowerCommand implements Command {
	
	private TV theTV;
	
	public TVPowerCommand ( TV someTV ) {
		theTV = someTV ;
	}
	
	public void execute() {
		theTV.power();
	}

}
