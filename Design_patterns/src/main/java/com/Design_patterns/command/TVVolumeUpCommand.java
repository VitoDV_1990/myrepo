package com.Design_patterns.command;

public class TVVolumeUpCommand implements Command {
	
	private TV theTV;
	
	public TVVolumeUpCommand ( TV someTV ) {
		theTV = someTV ;
	}
	
	public void execute() {
		theTV.volumeUp();
	}

}
