package com.Design_patterns.command;

public interface Command {
	
	public abstract void execute ( );

}
