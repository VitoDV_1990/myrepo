package com.Design_patterns.command;

public class TVVolumeDownCommand implements Command {
	
	private TV theTV;
	
	public TVVolumeDownCommand ( TV someTV ) {
		theTV = someTV ;
	}
	
	public void execute() {
		theTV.volumeDown();
	}

}
