package com.Design_patterns.mediator;

public abstract class BankEntity {
	
	BankServiceOrganizer bOrganizer;
	
	public BankEntity( BankServiceOrganizer bso ) {
		bOrganizer = bso;
	}
	
	public void changed() {
		bOrganizer.entityChanged( this );
	}

}
