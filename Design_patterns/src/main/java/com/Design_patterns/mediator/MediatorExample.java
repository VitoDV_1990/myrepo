package com.Design_patterns.mediator;

public class MediatorExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LittleBank bank = new LittleBank( );
		
		bank.showStatus();
		System.out.println( "A customer arrives to queue 1" );
		bank.customerArriveToQ1();
		bank.showStatus();
		System.out.println( "A customer arrives to queue 1" );
		bank.customerArriveToQ1();
		bank.showStatus();
		System.out.println( "A customer arrives to queue 1" );
		bank.customerArriveToQ1();
		bank.showStatus();
		System.out.println( "A customer arrives to queue 2" );
		bank.customerArriveToQ2();
		bank.showStatus();
		System.out.println( "End of service teller 1" );
		bank.endServiceTeller1();
		bank.showStatus();
		System.out.println( "End of service teller 2" );
		bank.endServiceTeller2();
		bank.showStatus();
		System.out.println( "End of service teller 2" );
		bank.endServiceTeller2();
		bank.showStatus();

	}

}
