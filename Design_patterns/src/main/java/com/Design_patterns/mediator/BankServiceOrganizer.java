package com.Design_patterns.mediator;

public interface BankServiceOrganizer {
	
	public void entityChanged( BankEntity be );

}
