package com.Design_patterns.factoryMethod;

public class ConnectorHandler extends ElementHandler{
	
	public MapElement newElement() {
		return new Connector();
	}
	
	public void connect(Connector conn, Place origin, Place destination) {
		conn.setPlacesConnected( origin, destination );
	}
	
}
