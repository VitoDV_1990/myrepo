package com.Design_patterns.factoryMethod;

public class PlaceHandler extends ElementHandler {
	
	public MapElement newElement() {
		return new Place();
	}

}
