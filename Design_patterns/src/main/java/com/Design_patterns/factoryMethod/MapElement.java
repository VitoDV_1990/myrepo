package com.Design_patterns.factoryMethod;

public interface MapElement {
	
	public abstract void setLabel( String id );
	
	public abstract String getPaintingData();

}
