package com.Design_patterns.factoryMethod;

import java.io.IOException;

public class FactoryMethodExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Creates the tools for handling elements
		ConnectorHandler cTool = new ConnectorHandler();
		PlaceHandler pTool = new PlaceHandler();
		
		// Vars
		Place startPoint = null, endPoint = null;
		Connector route = null;
		
		// Creates two places and one connector
		System.out.println( "1st. place creation" );
		try {
			startPoint = (Place) pTool.createElement();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println( "2nd. place creation" );
		try {
			endPoint = (Place) pTool.createElement();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println( "Connector creation" );
		try {
			route = (Connector) cTool.createElement();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Links places with the connection
		cTool.connect( route, startPoint , endPoint );
		
		// Paints the entire map
		pTool.paintElement( startPoint );
		pTool.paintElement( endPoint );
		cTool.paintElement( route );
	}

}
