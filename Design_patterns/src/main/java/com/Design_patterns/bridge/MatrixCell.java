package com.Design_patterns.bridge;

public class MatrixCell {
	
	public int row, col, value;
	
	public MatrixCell( int r, int c ) {
		row = r;
		col = c;
		value = 0;
	}

}
