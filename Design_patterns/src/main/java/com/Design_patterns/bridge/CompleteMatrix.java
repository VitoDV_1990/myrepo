package com.Design_patterns.bridge;

import java.util.Collection;

public class CompleteMatrix extends Matrix {
	
	public CompleteMatrix( int rows, int cols, @SuppressWarnings("rawtypes") Collection collection ) throws MatrixIndexOutOfBoundsException {
		super( rows, cols, collection );
		for(int i = 0 ; i< rows; i++ )
			for(int j = 0 ; j< cols; j++ )
				try {
					createPosition( i, j );
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
		
	public void put( int row, int col, int value ) throws MatrixIndexOutOfBoundsException{
		MatrixCell cell = getPosition( row, col );
		cell.value = value;
	}
	
	public int get( int row, int col ) throws MatrixIndexOutOfBoundsException {
		MatrixCell cell = getPosition( row, col );
		return cell.value;
	}

}
