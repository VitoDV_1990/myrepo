package com.Design_patterns.bridge;

import java.util.ArrayList;
import java.util.Vector;

public class BridgeExample {

	public static final int ROWS = 3;
	public static final int COLS = 4;
	
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CompleteMatrix matrixCV = null;
		try {
			matrixCV = new CompleteMatrix( ROWS, COLS, new Vector() );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println( "Complete Matrix with Vector:");
		try {
			matrixCV.put( 1, 2, 1 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixCV.put( 2, 1, 2 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixCV.put( 0, 3, 3 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixCV.put( 1, 2, 0 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0 ; i< ROWS; i++ ) {
			for(int j = 0 ; j< COLS; j++ )
				try {
					System.out.print( matrixCV.get( i, j )+" " );
				} catch (MatrixIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			System.out.println();
		}
		
		SparseMatrix matrixSV = new SparseMatrix( ROWS, COLS, new Vector() );
		System.out.println( "Sparse Matrix with Vector:");
		try {
			matrixSV.put( 1, 2, 1 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSV.put( 2, 1, 2 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSV.put( 0, 3, 3 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSV.put( 1, 2, 0 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0 ; i< ROWS; i++ ) {
			for(int j = 0 ; j< COLS; j++ )
				try {
					System.out.print( matrixSV.get( i, j )+" " );
				} catch (MatrixIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			System.out.println();
		}
		
		CompleteMatrix matrixCA = null;
		try {
			matrixCA = new CompleteMatrix( ROWS, COLS, new ArrayList() );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println( "Complete Matrix with ArrayList:");
		try {
			matrixCA.put( 1, 2, 1 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixCA.put( 2, 1, 2 );
		} catch (MatrixIndexOutOfBoundsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			matrixCA.put( 0, 3, 3 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixCA.put( 1, 2, 0 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0 ; i< ROWS; i++ ) {
			for(int j = 0 ; j< COLS; j++ )
				try {
					System.out.print( matrixCA.get( i, j )+" " );
				} catch (MatrixIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			System.out.println();
		}
		
		SparseMatrix matrixSA = new
		SparseMatrix( ROWS, COLS, new ArrayList() );
		System.out.println( "Sparse Matrix with ArrayList:");
		try {
			matrixSA.put( 1, 2, 1 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSA.put( 2, 1, 2 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSA.put( 0, 3, 3 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			matrixSA.put( 1, 2, 0 );
		} catch (MatrixIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i = 0 ; i< ROWS; i++ ) {
			for(int j = 0 ; j< COLS; j++ )
				try {
					System.out.print( matrixSA.get( i, j )+" " );
				} catch (MatrixIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			System.out.println();
		}
		
	}
}
