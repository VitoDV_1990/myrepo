package com.Design_patterns.flyWeight;

public class FlyweightExternalStateExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println( "Exemplifies the external state usage." );
		EventSymbol evnt;
		
		for(int i=0 ; i < 10; i++) {
			String owner = Math.random()< .5 ? "Generator A" : "Generator B";
			double r = Math.random();
			if(r < .3)
				evnt = EventSymbolFactory.newEventSymbol( "EVENT 1" );
			else if( r < .6 )
				evnt = EventSymbolFactory.newEventSymbol( "EVENT 2" );
			else
				evnt = EventSymbolFactory.newEventSymbol( "EVENT 3" );
		evnt.print( owner );
		}

	}

}
