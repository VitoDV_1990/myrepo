package com.Design_patterns.flyWeight;

import java.util.Hashtable;

public class EventSymbolFactory {
	
	private static Hashtable events = new Hashtable();
	
	public static EventSymbol newEventSymbol( String evnt ) {
		EventSymbol evntRef = (EventSymbol) events.get( evnt );
		if( evntRef == null ) {
			evntRef = new EventSymbol( evnt );
			events.put( evnt, evntRef );
		}
	return evntRef;
	}

}
